import useAuth from "../hooks/useAuth"

export default function Home() {
  const { user, signin } = useAuth();
  console.log('user', user);
  return (
    <div>
      <h2>Criando uma app do zero com NEXTJS.</h2>
      <p>por: Danilo Righetto</p>
      <button onClick={() => signin()}>Entrar com Github</button>
    </div>
  )
}
