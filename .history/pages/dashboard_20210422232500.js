import useAuth from './../hooks/useAuth';
function Dashboard() {
    const { user, signin } = useAuth();
    return (<h1>Dashboard: {user}</h1>)
}

export default Dashboard;