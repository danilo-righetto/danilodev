import { createContext, useState } from 'react'

const AuthContext = createContext();

export function AuthProvider({ childern }) {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);
    return (
        <h1>Provider</h1>
    )
};

export const AuthConsumer = AuthContext.Consumer;

export default AuthContext;