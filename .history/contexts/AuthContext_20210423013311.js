import Router from 'next/dist/next-server/lib/router/router';
import { createContext, useState } from 'react'

const AuthContext = createContext();

export function AuthProvider({ childern }) {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    const signin = () => {
        try {
            setLoading(true);
            return firebase
            .auth()
            .signInWithPopup(new firebase.auth.GithubAuthProvider())
            .then((response) => {
                setUser(response.user);
                Router.push('/dashboard');
            });
        } finaly {
            setLoading(false);
        }
    }
    const signout = () => {}
    return (
        <AuthContext.Provider value={{
            user,
            loading,
            signin,
            singout
        }}>{children}</AuthContext.Provider>
    )
};

export const AuthConsumer = AuthContext.Consumer;

export default AuthContext;