import { createContext, useState } from 'react'

const AuthContext = createContext();

export function AuthProvider({ childern }) {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    const signin = () => {
        setLoading(true);
        return firebase;
    }
    const signout = () => {}
    return (
        <AuthContext.Provider value={{
            user,
            loading,
            signin,
            singout
        }}>{children}</AuthContext.Provider>
    )
};

export const AuthConsumer = AuthContext.Consumer;

export default AuthContext;