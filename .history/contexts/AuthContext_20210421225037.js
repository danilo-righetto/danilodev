import { createContext, useState } from 'react'

const AuthContext = createContext();

export function AuthProvider({ childern }) {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);
    return (
        <AuthContext.Provider value={{
            user,
            loading,
            signIn,
            singout
        }}>{children}</AuthContext>
    )
};

export const AuthConsumer = AuthContext.Consumer;

export default AuthContext;